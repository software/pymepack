Lyapunov and Stein Equations
============================

.. currentmodule:: pymepack

Standard Lyapunov and Stein Equations
_____________________________________

.. autofunction:: gelyap
.. autofunction:: gelyap_refine
.. autofunction:: gestein
.. autofunction:: gestein_refine

Generalized Lyapunov and Stein Equations
________________________________________

.. autofunction:: gglyap
.. autofunction:: gglyap_refine
.. autofunction:: ggstein
.. autofunction:: ggstein_refine

Residual Computations
_____________________

.. autofunction:: res_gelyap
.. autofunction:: res_gglyap
.. autofunction:: res_gestein
.. autofunction:: res_ggstein


