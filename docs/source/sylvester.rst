Sylvester Equations
============================

.. currentmodule:: pymepack

Standard Sylvester Equations
_____________________________________

.. autofunction:: gesylv
.. autofunction:: gesylv_refine
.. autofunction:: gesylv2
.. autofunction:: gesylv2_refine

Generalized Sylvester Equations
________________________________________
.. |_| unicode:: 0xA0
.. autofunction:: ggsylv
.. autofunction:: ggsylv_refine
.. autofunction:: ggcsylv
.. autofunction:: ggcsylv_refine
.. autofunction:: ggcsylv_dual
.. autofunction:: ggcsylv_dual_refine

Residual Computations
_____________________
.. autofunction:: res_gesylv
.. autofunction:: res_gesylv2
.. autofunction:: res_ggsylv
.. autofunction:: res_ggcsylv
.. autofunction:: res_ggcsylv_dual

